import { View, Text } from 'react-native'
import React from 'react'

const WelcomeScreen = (props) => {
  const appIcons = props.route.params.appIcons;

  return (
    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
      <appIcons.isaDate height={170} width={170} />
      <Text style={{ marginTop: 20, fontSize: 20, color: '#0F0140' }}> ******* Demo App ******* </Text>
    </View>
  )
}

export default WelcomeScreen
