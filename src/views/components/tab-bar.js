import React from 'react'
import { View, FlatList, Dimensions, TouchableOpacity } from 'react-native'

const TabBar = ({ state, appIcons, navigation }) => {
  const tabIconsList = [
    {
      id: 0,
      focusedIcon: <appIcons.homeSelect height={30} width={30} />,
      notFocusedIcon: <appIcons.homeUnselect height={30} width={30} />,
      label: 'Home',
      route: state.routes[0],
      isFocused: state.index === 0,
    },
    {
      id: 1,
      focusedIcon: <appIcons.transactionSelect height={30} width={30} />,
      notFocusedIcon: <appIcons.transactionUnselect height={30} width={30} />,
      label: 'Chat',
      route: state.routes[1],
      isFocused: state.index === 1,
    },

    {
      id: 2,
      focusedIcon: <appIcons.budgetSelect height={30} width={30} />,
      notFocusedIcon: <appIcons.budgetUnselect height={30} width={30} />,
      label: 'Notification',
      route: state.routes[2],
      isFocused: state.index === 2,
    },

  ];

  const onPress = (isFocused, route) => {
    const event = navigation.emit({
      type: 'tabPress',
      target: route.key,
      canPreventDefault: true,
    });
    if (!isFocused && !event.defaultPrevented) {
      navigation.navigate({ name: route.name, merge: true });
    }
  };

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => onPress(item.isFocused, item.route)}>
        <View style={{ height: 50, width: Dimensions.get('window').width / 3, justifyContent: 'center', backgroundColor: 'white', }}>
          <View style={{ alignItems: 'center', }}>
            {item.isFocused ? item.focusedIcon : item.notFocusedIcon}
            <Text style={{ fontSize: 13, color: '#400808', marginTop: 4 }}>{item.label}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View>
      <FlatList
        horizontal
        bounces={false}
        data={tabIconsList}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};
export default TabBar