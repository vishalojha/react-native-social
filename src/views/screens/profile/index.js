import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, PermissionsAndroid } from 'react-native'
import { request, PERMISSIONS } from 'react-native-permissions';


export async function requestMultiplePermission() {
  try {
    return PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.CALL_PHONE,
      PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      PermissionsAndroid.PERMISSIONS.READ_MEDIA_IMAGES,
      PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
    ]
    )
  } catch (error) { }
}

const Profile = () => {
  // const askForPermission = (permission) => {
  //   request(permission).then((result) => {
  //     console.log('result >>>', result)
  //   });
  // }

  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity style={styles.buttonWrap} onPress={() => {
        askForPermission(PERMISSIONS.ANDROID.CAMERA)
      }}>
        <Text>Camera Permission</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonWrap} onPress={() => {
        askForPermission(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES)
      }}>
        <Text>Image Gallery Permission</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonWrap} onPress={() => {
        askForPermission(PERMISSIONS.ANDROID.READ_CONTACTS)
      }}>
        <Text>Contact Permission</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.buttonWrap} onPress={() => {
        askForPermission(PERMISSIONS.ANDROID.ACCESS_BACKGROUND_LOCATION)
      }}>
        <Text>Location Permission</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Profile

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 15,
    justifyContent: 'center',
  },
  buttonWrap: {
    width: '90%',
    height: 50,
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});