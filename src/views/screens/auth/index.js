import { View, TouchableOpacity, StyleSheet, Platform } from 'react-native'
import React, { useEffect } from 'react'
import appIcons from 'utils/icons'
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { requestUserPermission } from 'utils/notificationServices';
import { requestMultiplePermission } from '../profile';



const LoginMain = () => {
  const navigation = useNavigation();

  // Push notification useEffect
  useEffect(() => {
    // requestUserPermission()
  }, [])

  // Login with Google:- 
  useEffect(() => {
    GoogleSignin.configure({
      offlineAccess: true,
      scopes: ['profile', 'email'],
      webClientId: '416452313404-um0pc339mve0m1hi8ahoclp3raedmb2c.apps.googleusercontent.com',
    });

  }, []);

  const onGoogleSignIn = async () => {
    await GoogleSignin.hasPlayServices();
    const userInfo = await GoogleSignin.signIn();
    console.log('userInfo>>>>', userInfo)

    const googleCredential = auth.GoogleAuthProvider.credential(userInfo);

    return auth().signInWithCredential(googleCredential);
  }


  // Sign Out 
  const signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      alert('Your are signed out!')
    } catch (error) {
      console.error(error.toString());
    }
  };

  return (
    <View style={styles.mainContainer}>

      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={styles.socialWrap}>
          <TouchableOpacity onPress={() => onGoogleSignIn()} activeOpacity={0.7}>
            <appIcons.googleIcon width={30} height={30} />
          </TouchableOpacity>
        </View>

        <View style={styles.socialWrap}>
          <TouchableOpacity activeOpacity={0.7}>
            <appIcons.facebookIcon width={30} height={30} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={{ marginTop: 20 }}>
        <Button title='LogOut Google.!' onPress={() => signOut()} />
      </View>
      <View style={{ marginTop: 20 }}>
        <Button title='Go to home' onPress={() => navigation.navigate("HomeScreen")} />
      </View>
      <View style={{ marginTop: 20 }}>
        <Button title='Go to Profile' onPress={() => navigation.navigate("ProfileScreen")} />
      </View>

    </View>
  )
}

export default LoginMain;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  socialWrap: {
    width: 50,
    height: 50,
    borderWidth: 1.5,
    borderRadius: 50,
    marginHorizontal: 10,
    alignItems: 'center',
    borderColor: '#bfbfbf',
    justifyContent: 'center',
  },
});


  // Push Notification old code 
  // useEffect(() => {
  //   getDeviceToken();
  // }, [])

  // const getDeviceToken = async () => {
  //   let token = await messaging().getToken();
  //   console.log('token>>>', token)
  // };

  // useEffect(() => {
  //   const unsubscribe = messaging().onMessage(async remoteMessage => {
  //     console.log('remoteMessage>>', remoteMessage)
  //     displayNotification(remoteMessage)
  //   });
  //   return unsubscribe;
  // }, []);


  // const displayNotification = async (data) => {
  //   await notifee.requestPermission();
  //   // Create a channel (required for Android)
  //   const channelId = await notifee.createChannel({
  //     id: 'default',
  //     name: 'Default Channel',
  //   });
  //   // Display a notification
  //   await notifee.displayNotification({
  //     title: data.notification.title,
  //     body: data.notification.body,
  //     android: {
  //       channelId,
  //       pressAction: {
  //         id: 'default'
  //       },
  //       // style: {
  //       //   type: AndroidStyle.BIGPICTURE,
  //       //   picture: '',
  //       // },
  //     }
  //   })
  // }

  // Permissioin
  // useEffect(() => {
  //   try {
  //     if (Platform.OS == 'android' && DeviceInfo.getApiLevelSync() >= 30) {
  //
  // PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.POST_NOTIFICATION).then(res => {
  //         // if (!!res && res == 'granted') {
  //         // }
  //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //           permissionGranted = true
  //         }
  //         requestUserPermission()
  //         notificationListeners()
  //       }).catch(err => {
  //         alert('Something wrong.!')
  //       })
  //     }
  //   } catch (error) {
  //   }
  // }, [])