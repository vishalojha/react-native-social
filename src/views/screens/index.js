import Home from "./home";
import LoginMain from "./auth";
import Profile from "./profile";
import ChatMain from "./chat/chat-main";

export {

  Home,
  Profile,
  ChatMain,
  LoginMain,
}