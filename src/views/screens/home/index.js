import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react'
import { View, Text, Button, StyleSheet, TouchableOpacity } from 'react-native'
import dynamicLinks from '@react-native-firebase/dynamic-links';
import Clipboard from '@react-native-clipboard/clipboard';
import OneSignal from 'react-native-onesignal';



const Home = () => {

  // OneSignal Initialization
  // Your App ID: 3f458baa-41bf-4274-b2be-97e0273cd9e1
  OneSignal.setAppId('3f458baa-41bf-4274-b2be-97e0273cd9e1');

  OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
    console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);

    let notification = notificationReceivedEvent.getNotification();
    console.log("notification: ", notification);

    const data = notification.additionalData
    console.log("additionalData: ", data);

    notificationReceivedEvent.complete(notification);
  });

  OneSignal.setNotificationOpenedHandler(notification => {
    console.log("OneSignal: notification opened:", notification);    
  });


  const navigation = useNavigation();
  const [generateLink, setGenerateLink] = useState('')

  // Open App from click on link: 
  // const buildLink = async () => {
  //   const link = await dynamicLinks().buildLink({
  //     link: 'https://piecodes.in',
  //     domainUriPrefix: 'https://isadate.page.link',
  //     analytics: {
  //       campaign: 'banner',
  //     },
  //   });

  //   setGenerateLink(link);
  // }


  return (
    <View style={styles.mainContainer}>
      {/* <View style={{ marginVertical: 20, alignItems: 'center', paddingHorizontal: 15 }}>
        <Text>{generateLink}</Text>

        <TouchableOpacity onPress={() => { buildLink() }} style={styles.generateLinkBtn}>
          <Text>Generate deep Link</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => { Clipboard.setString(generateLink) }} style={styles.generateLinkBtn}>
          <Text>Copy deep Link</Text>
        </TouchableOpacity>
      </View>
      <Button title='Go to Back' onPress={() => navigation.navigate("LoginScreen")} /> */}

    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generateLinkBtn: {
    width: 200,
    height: 50,
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});