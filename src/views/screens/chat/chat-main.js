import React, { useEffect, useState } from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ChatList from './chat-screen/chat-list';
import GroupList from './group-screen/group-list';
import appIcons from 'utils/icons';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { getDeviceId } from 'utils/helpers';

const Tab = createMaterialTopTabNavigator();

const ChatMain = () => {
  const navigation = useNavigation()

  const [userId, setUserId] = useState()
  console.log('userId', userId)
  const [showModal, setShowModal] = useState(false)

  useEffect(() => {
    getDeviceId().then(id => setUserId(id));
  }, [])


  return (
    <>
      {/* Header */}
      <View style={styles.headerWrap}>
        <Text style={styles.textStyle}>Chat App</Text>

        <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('AddFriendScreen', { appIcons: appIcons })}>
          <appIcons.addFriendIcon />
        </TouchableOpacity>
      </View>


      {/* Tab Navigator */}
      <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: { fontSize: 14, fontWeight: 'bold' },
          // tabBarIndicatorStyle: { backgroundColor: '#d8d8d8' },
        }}>

        <Tab.Screen
          name="Chats"
          component={ChatList}
          options={{ tabBarLabel: 'Chats' }}

        />

        <Tab.Screen
          name="Group"
          component={GroupList}
          options={{ tabBarLabel: 'Groups' }}

        />

      </Tab.Navigator>


      {/* Add Chat-Group button */}
      <View style={styles.modalStyle}>
        {showModal &&
          <>
            <View style={styles.bottomSheetBlackMask} />
            <TouchableOpacity style={styles.generateLinkBtn} onPress={() => { setShowModal(!showModal); navigation.navigate('NewChatScreen', { appIcons: appIcons, userId: userId }) }} >
              <Text style={styles.textStyle}>New Chat</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.generateLinkBtn} onPress={() => { setShowModal(!showModal); navigation.navigate('NewGroupScreen', { appIcons: appIcons, userId: userId }) }}>
              <Text style={styles.textStyle}>New Group</Text>
            </TouchableOpacity>
          </>}

        <TouchableOpacity activeOpacity={0.7} onPress={() => { setShowModal(!showModal) }} style={{ marginTop: 15 }}>
          <appIcons.addBtn />
        </TouchableOpacity>
      </View>
    </>
  )
}

export default ChatMain

const styles = StyleSheet.create({
  bottomSheetBlackMask: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  modalStyle: {
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    paddingEnd: 20,
    paddingBottom: 40,
    position: 'absolute',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  headerWrap: {
    height: 70,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
    borderBottomWidth: 1.5,
    borderColor: '#d8d8d8',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  generateLinkBtn: {
    width: 120,
    height: 40,
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    borderColor: 'rgba(64, 8, 8, 1)',
  },
  textStyle: {
    fontSize: 16,
    fontWeight: '700',
    color: 'rgba(64, 8, 8, 1)'
  },
})


  // < TouchableOpacity activeOpacity = { 0.7} onPress = {() => { setShowModal(!showModal) }} style = {{ position: 'absolute', bottom: 25, right: 25 }}>
  //   <appIcons.addBtn />
  //     </TouchableOpacity >

  // { showModal &&
  // <>
  //   {/* <View style={styles.bottomSheetBlackMask} /> */}
  //   <View style={{ alignItems: 'flex-end', height: 130, width: 150, position: 'absolute', bottom: 80, right: 25 }}>

  //     <TouchableOpacity style={styles.generateLinkBtn} onPress={() => setShowModal(!showModal)} >
  //       <Text style={styles.textStyle}>New Chat</Text>
  //     </TouchableOpacity>

  //     <TouchableOpacity style={styles.generateLinkBtn} onPress={() => setShowModal(!showModal)}>
  //       <Text style={styles.textStyle}>New Group</Text>
  //     </TouchableOpacity>
  //   </View>
  // </>
  //     }