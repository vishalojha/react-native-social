import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import { generateLetter } from 'utils'
import { getImage } from 'utils/helpers'

const UserChatHeader = ({ appIcons, contactUserRef }) => {
  const navigation = useNavigation();
  const [user, setUser] = useState({});

  useEffect(() => {
    getImage();
    getContactData()
  }, [])

  const getContactData = async () => {
    const contactSnapShot = await contactUserRef.get();
    const data = contactSnapShot.data();
    const name = data?.name
    const profile = await getImage(data?.profile)
    setUser({ name, profile })
  }

  return (
    <View style={styles.mainContainer}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <appIcons.backButton />
      </TouchableOpacity>

      <View style={styles.imageStyle}>
        {user?.profile ?
          (<Image source={{ uri: user?.profile }} style={{ height: 40, width: 40, borderRadius: 100 }} />)
          :
          (<Text style={styles.generateLetterStyle}>{generateLetter(user?.name)}</Text>)
        }
      </View>

      <Text style={{ marginStart: 10, fontSize: 15, color: '#000', fontWeight: '700' }}>{user?.name}</Text>
    </View>
  )
}

export default UserChatHeader

const styles = StyleSheet.create({
  mainContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    borderColor: '#CCE0DF',
    backgroundColor: 'rgba(204, 224, 223, 0.4)',
  },
  imageStyle: {
    width: 35,
    height: 35,
    borderWidth: 1,
    marginStart: 10,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generateLetterStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.7)',
  },

})