import React, { useState } from 'react'
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native'
import UserChatHeader from '../userChatHeader'
import firestore from '@react-native-firebase/firestore';
import UserChatBody from './user-chat-body';


const UserChat = (props) => {
  const { appIcons, contactId, userId } = props.route.params;

  const [messages, setMessages] = useState('');
  const [sendEnable, setSendEnable] = useState(false);

  const onchange = (value) => {
    setMessages(value)
    setSendEnable(true)
  }

  const onSend = () => {
    chatRef.collection('messages').add({
      body: messages,
      sender: userId,
      timestamp: firestore.FieldValue.serverTimestamp()
    })
    setMessages('')
    setSendEnable(false)
  }

  const generateChatId = () => {
    const sorteduserId = [userId, contactId].sort();
    const chatId = sorteduserId.join('_')
    return chatId
  }

  const chatId = generateChatId();
  const chatRef = firestore().collection('chats').doc(chatId)
  const userRef = firestore().collection('users').doc(userId)
  const contactUserRef = firestore().collection('users').doc(contactId)

  const createChatRoom = async () => {
    const chatSnapShot = await chatRef.get();
    if (!chatSnapShot.exists) {
      const participants = [userRef, contactUserRef];
      await chatRef.set({ participants });
    }
  };

  createChatRoom();

  return (
    <View style={styles.mainContainer}>
      <UserChatHeader appIcons={appIcons} contactUserRef={contactUserRef} />

      <UserChatBody chatId={chatId} userId={userId} />

      <View style={styles.textFieldWrapper}>
        <View style={styles.messageFieldMainContainer}>
          <View style={styles.textInputStyle}>
            <TextInput
              placeholder='Schedule through chat...'
              onChangeText={value => onchange(value)}
              value={messages}
            />
          </View>
        </View>

        <TouchableOpacity disabled={!sendEnable} onPress={onSend} activeOpacity={0.7} style={styles.sendIcon} >
          <appIcons.sendIcon height={36} width={36} />
        </TouchableOpacity>
      </View>
    </View>
  )
}


export default UserChat

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textFieldWrapper: {
    height: 80,
    padding: 12,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#CCE0DF',
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    backgroundColor: '#F8FCFF',
  },
  messageFieldMainContainer: {
    width: '88%',
    borderWidth: 1,
    borderRadius: 25,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#CCE0DF',
  },
  textInputStyle: {
    flex: 6,
    fontSize: 14,
    marginStart: 10,
  },
  sendIcon: {
    marginStart: 10,
  },
})