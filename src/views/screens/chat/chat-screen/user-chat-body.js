import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import firestore from '@react-native-firebase/firestore';


const UserChatBody = ({ chatId, userId }) => {
  const scrollViewRef = useRef();
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    firestore()
      .collection('chats')
      .doc(chatId)
      .collection('messages')
      .orderBy('timestamp')
      .onSnapshot(snapShot => {
        const allMessages = snapShot.docs.map(snap => {
          return snap.data();
        });
        setMessages(allMessages);
      });
  }, [])


  const UserMessageView = ({ message, time }) => {
    return (
      <View style={styles.userContainer}>
        <View style={styles.userInnerContainer}>
          <Text style={styles.message}>{message}</Text>
          <Text style={styles.time}>{time}</Text>
        </View>
      </View>
    );
  };

  const OtherUserMessageView = ({ message, time }) => {
    return (
      <View style={styles.otherUserContainer}>
        <View style={styles.otherUserInnerContainer}>
          <Text style={styles.message}>{message}</Text>
          <Text style={styles.time}>{time}</Text>
        </View>
      </View>
    );
  };

  const scrollToBottom = () => {
    scrollViewRef.current.scrollToEnd({ animated: true })
  }

  return (
    <View style={styles.mainContainer}>
      <ScrollView
        ref={scrollViewRef}
        onContentSizeChange={scrollToBottom}
        showsVerticalScrollIndicator={false}>
        {messages.map(item => (
          <>
            {item.sender === userId ? (
              <UserMessageView
                message={item.body}
                time={item?.timestamp?.toDate()?.toDateString()}
              />
            ) : (
              <OtherUserMessageView
                message={item.body}
                time={item?.timestamp?.toDate()?.toDateString()}
              />
            )}
          </>
        ))}
      </ScrollView>
    </View>
  )
}

export default UserChatBody

const styles = StyleSheet.create({

  mainContainer: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
  },
  userContainer: {
    marginVertical: 8,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  userInnerContainer: {
    paddingVertical: 8,
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'flex-end',
    borderTopLeftRadius: 30,
    backgroundColor: '#36897f',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  otherUserContainer: {
    marginVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  otherUserInnerContainer: {
    paddingVertical: 8,
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'flex-end',
    borderTopRightRadius: 30,
    backgroundColor: '#232d36',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  message: {
    fontSize: 13,
    color: '#ffffff',
  },
  time: {
    fontSize: 9,
    color: '#ffffff',
    marginLeft: 5,
  },

})