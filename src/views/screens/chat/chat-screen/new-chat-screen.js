import React, { useEffect, useState } from 'react'
import { generateLetter } from 'utils';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import firestore from '@react-native-firebase/firestore';

import { getImage } from 'utils/helpers';
import { useNavigation } from '@react-navigation/native';

const NewChatScreen = (props) => {
  const { appIcons, userId } = props.route.params

  const navigation = useNavigation()
  const [users, setUsers] = useState([])

  useEffect(() => {
    getImage();
    getUserData()
      .then(res => setUsers(res))
      .catch(error => console.log('error :', error));
  }, []);

  const getUserData = async () => {
    const userRef = await firestore().collection('users').get();
    const userData = Promise.all(
      userRef.docs.filter(item => {
        return item.id != userId
      }).map(async item => {
        const id = item.id;
        const name = item.data().name;
        const profile = await getImage(item.data().profile);
        return { id, name, profile }
      }));
    return userData
  };

  const onNavigate = (contactId) => {
    navigation.navigate('UserChatScreen', {
      userId: userId,
      appIcons: appIcons,
      contactId: contactId,
    });
  }

  const renderItem = ({ item, index }) => {
    return (
      <>
        <TouchableOpacity onPress={() => onNavigate(item.id)} activeOpacity={0.6} key={index} style={styles.chatWrapper}>
          <View style={styles.imageStyle}>
            {item?.profile ?
              (<Image source={{ uri: item.profile }} style={{ height: 40, width: 40, borderRadius: 100 }} />)
              :
              (<Text style={styles.generateLetterStyle}>{generateLetter(item?.name)}</Text>)
            }
          </View>

          <View style={styles.rightWrap}>
            <Text style={styles.nameStyle}>{item?.name}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.borderStyle} />
      </>
    );
  };

  return (
    <View style={styles.mainContainer}>
      <FlatList
        bounces={false}
        data={users}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  )
}

export default NewChatScreen

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  chatWrapper: {
    marginVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  imageStyle: {
    width: 40,
    height: 40,
    borderWidth: 1,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generateLetterStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  rightWrap: {
    flex: 1,
    marginStart: 15
  },
  nameStyle: {
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
    fontWeight: '700',
  },
  userIdText: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.6)',
  },
  borderStyle: {
    marginVertical: 5,
    borderBottomWidth: 1.5,
    borderBottomColor: '#D8D8D8',
  },
  listHeaderStyle: {
    marginVertical: 15,
    paddingHorizontal: 20,
  },
  headerText: {
    fontSize: 20,
    color: '#121212',
    fontWeight: '700',
  },
})