import { useNavigation } from '@react-navigation/native';
import React, { useEffect } from 'react'
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { generateLetter } from 'utils';
import appIcons from 'utils/icons';

const ChatList = () => {
  const navigation = useNavigation()

  const chatListing = [
    {
      id: 0,
      name: 'Abhay Saed',
      description: 'Dude, that is so funny',
      dateTime: '10:04 am',
      avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 1,
      name: 'Charles X',
      description: 'Yooo! See you guys soon!',
      dateTime: '10:00 am',
      avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 2,
      name: 'John Doe',
      description: 'Hey, guys chess this game',
      dateTime: 'yesterday',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 3,
      name: 'Bobby Max',
      description: 'Dude, that is so funny',
      dateTime: '28/06/2023',
      avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 4,
      name: 'Marry Pops',
      description: 'Hey, guys chess this game',
      dateTime: '25/07/2023',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
  ];

  const renderItem = ({ item, index }) => {
    return (
      <>
        <TouchableOpacity onPress={() => navigation.navigate('UserChatScreen', { appIcons: appIcons, navigation: navigation })} activeOpacity={0.7} key={index} style={styles.chatWrapper}>
          <View style={styles.imageStyle}>
            {item?.avtarIcon ? (item?.avtarIcon) :
              <Text style={styles.generateLetterStyle}>{generateLetter(item?.name)}</Text>}
          </View>

          <View style={styles.rightWrap}>
            <View style={styles.nameWrap}>
              <Text style={styles.nameStyle}>{item?.name}</Text>
              <Text style={styles.descriptionStyle}>{item?.dateTime}</Text>
            </View>

            <Text style={[styles.descriptionStyle, { color: 'rgba(0, 0, 0, 1)' }]}>{item?.description}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.borderStyle} />
      </>
    );
  };

  return (
    <View style={styles.mainContainer}>
      <FlatList
        bounces={false}
        data={chatListing}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  )
}

export default ChatList

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  chatWrapper: {
    marginVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  imageStyle: {
    width: 40,
    height: 40,
    borderWidth: 1,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generateLetterStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  rightWrap: {
    flex: 1,
    marginStart: 15
  },
  nameWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameStyle: {
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
    fontWeight: '700',
  },
  descriptionStyle: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.6)',
  },
  borderStyle: {
    marginVertical: 5,
    borderBottomWidth: 1.5,
    borderBottomColor: '#D8D8D8',
  },
})