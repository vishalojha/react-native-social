import React from 'react'
import { generateLetter } from 'utils';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const AddFriend = (props) => {

  const navigation = props.navigation
  const appIcons = props.route.params.appIcons


  const friendListing = [
    {
      id: 0,
      name: 'Abhay Saed',
      userId: '@abhay.saed',
      // avtarIcon: <appIcons.isaDate height={38} width={38} />,
    },
    {
      id: 1,
      name: 'Charles X',
      userId: '@charles.x',
      avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 2,
      name: 'John Doe',
      userId: '@john.doe',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 3,
      name: 'Bobby Max',
      userId: '@bobby.max',
      avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 4,
      name: 'Marry Pops',
      userId: '@marry.pops',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 5,
      name: 'Liam Matthew',
      userId: '@liam.matt',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
    {
      id: 6,
      name: 'Jon Snow',
      userId: '@jon.snow',
      // avtarIcon: <appIcons.userAvtar height={38} width={38} />,
    },
  ];


  const renderItem = ({ item, index }) => {
    return (
      <>
        <TouchableOpacity activeOpacity={0.6} key={index} style={styles.chatWrapper}>
          <View style={styles.imageStyle}>
            {item?.avtarIcon ? (item?.avtarIcon) :
              <Text style={styles.generateLetterStyle}>{generateLetter(item?.name)}</Text>}
          </View>

          <View style={styles.rightWrap}>
            <Text style={styles.nameStyle}>{item?.name}</Text>
            <Text style={[styles.userIdText, { color: 'rgba(0, 0, 0, 1)' }]}>{item?.userId}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.borderStyle} />
      </>
    );
  };


  return (
    <View style={styles.mainContainer}>

      <FlatList
        bounces={false}
        data={friendListing}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ListHeaderComponent={
          <View style={styles.listHeaderStyle}>
            <Text style={styles.headerText}>Friend List</Text>
          </View>
        }
      />
    </View>
  )
}

export default AddFriend

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  chatWrapper: {
    marginVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  imageStyle: {
    width: 40,
    height: 40,
    borderWidth: 1,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  generateLetterStyle: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.7)',
  },
  rightWrap: {
    flex: 1,
    marginStart: 15
  },
  nameStyle: {
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
    fontWeight: '700',
  },
  userIdText: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: '500',
    color: 'rgba(0, 0, 0, 0.6)',
  },
  borderStyle: {
    marginVertical: 5,
    borderBottomWidth: 1.5,
    borderBottomColor: '#D8D8D8',
  },
  listHeaderStyle: {
    marginVertical: 15,
    paddingHorizontal: 20,
  },
  headerText: {
    fontSize: 20,
    color: '#121212',
    fontWeight: '700',
  },
})