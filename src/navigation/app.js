import React from 'react'
import { View } from 'react-native'
import { LogBox } from 'react-native';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';

import { ChatMain, Home, LoginMain, Profile } from 'views/screens';
import navigationService from 'utils/navigationService';
import AddFriend from 'views/screens/chat/add-friend';
import UserChat from 'views/screens/chat/chat-screen/user-chat';
import GroupChat from 'views/screens/chat/group-screen/group-chat';
import NewChatScreen from 'views/screens/chat/chat-screen/new-chat-screen';
import NewGroupScreen from 'views/screens/chat/group-screen/new-group-screen';


const Stack = createStackNavigator();

LogBox.ignoreLogs(['Non-serializable values were found in the navigation state'])

// const Auth = () => {
//   return (
//     <Stack.Navigator>
//       <Stack.Screen
//         name="LoginScreen"
//         component={LoginMain}
//         options={{ headerShown: false }}
//       />

//       <Stack.Screen
//         component={Home}
//         name="HomeScreen"
//       // options={{ headerShown: false }}
//       />

//       <Stack.Screen
//         component={Profile}
//         name="ProfileScreen"
//       // options={{ headerShown: false }}
//       />
//     </Stack.Navigator>
//   );
// };


const ChatApp = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{ headerShown: false }}
        component={ChatMain}
        name="ChatMainScreen"
      />

      <Stack.Screen
        component={UserChat}
        name="UserChatScreen"
        // options={{ title: 'User Chat', }}
      options={{ headerShown: false }}

      />

      <Stack.Screen
        component={GroupChat}
        name="GroupChatScreen"
        options={{ title: 'Group Chat', }}
      // options={{ headerShown: false }}

      />

      <Stack.Screen
        component={AddFriend}
        name="AddFriendScreen"
        options={{ title: 'Add Friends', headerTitleAlign: 'center', }}
      // options={{ headerShown: false }}

      />

      <Stack.Screen
        component={NewChatScreen}
        name="NewChatScreen"
        options={{ title: 'New Chat', headerTitleAlign: 'left', }}
      // options={{ headerShown: false }}

      />

      <Stack.Screen
        component={NewGroupScreen}
        name="NewGroupScreen"
        options={{ title: 'New Group', headerTitleAlign: 'left', }}
      // options={{ headerShown: false }}

      />
    </Stack.Navigator>
  );
};


const AppNavigator = () => {
  return (
    <View style={{ flex: 1 }}>
      <NavigationContainer ref={(ref) => navigationService.setTopLevelNavigator(ref)}>
        {/* <Auth /> */}
        <ChatApp />
      </NavigationContainer>
    </View>
  )
}

export default AppNavigator