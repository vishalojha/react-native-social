import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { Home } from 'views/screens';

const StackNavigator = () => {
  const Stack = createStackNavigator();
  // const doNotShowHeaderOption = {
  //   headerShown: false,
  // };

  return (
    <Stack.Navigator>

      <Stack.Screen
        options={{
          title: 'Home Screen',
          headerTitleAlign: 'center',
        }}
        name={'HomeScreen'}
        component={Home}
      />

    </Stack.Navigator>
  )
}

export default StackNavigator