import React from 'react'
import { SafeAreaView } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Home } from 'views/screens';
import { TabBar } from 'views/components';


const Tabs = createBottomTabNavigator();
const doNotShowHeaderOption = {
  headerShown: false,
};

const BottomNavigator = (props) => {
  const { appIcons } = props.route.params

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Tabs.Navigator
        tabBar={(props) => (
          <TabBar {...props} appIcons={appIcons} />
        )}
      >

        <Tabs.Screen
          name='HomeScreen'
          component={Home}
          initialParams={{
            appIcons: appIcons,
          }}
          options={doNotShowHeaderOption}
        />


      </Tabs.Navigator>
    </SafeAreaView>
  )
}

export default BottomNavigator