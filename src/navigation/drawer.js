import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import BottomNavigator from './bottom';

const Drawer = createDrawerNavigator();
const doNotShowHeaderOption = {
  headerShown: false,
};

const DrawerNavigator = (props) => {
  const { appIcons } = props.route.params;

  return (
    <Drawer.Navigator
      initialRouteName="BottomNavigator"
      // drawerContent={(props) =>
      //   DrawerContent({ props: props, appIcons: appIcons,})
      // }
      screenOptions={{
        drawerStyle: { borderTopRightRadius: 5, },
      }}
    >
      <Drawer.Screen
        name="Menu"
        component={BottomNavigator}
        initialParams={{
          appIcons: appIcons,
        }}
        options={doNotShowHeaderOption}
      />
    </Drawer.Navigator>
  );
};
export default DrawerNavigator;
