import { NavigationAction } from "@react-navigation/native";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.navigate(routeName, params)
}

export default {
  navigate,
  setTopLevelNavigator,
}