
export const generateAvatar = (first_name, last_name) => {
  const fullName = `${first_name} ${last_name}`;
  return fullName.replace(/(^\w{1})|(\s+\w{1})/g, (letter) => letter.toUpperCase());
};

export const generateLetter = (first_name) => {
  const fullName = `${first_name}`;
  return fullName.match(/\b(\w)/g)?.splice(0, 2).join('').toUpperCase()
}

