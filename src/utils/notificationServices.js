import { Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import navigationService from './navigationService';
import notifee, { AndroidImportance, EventType } from '@notifee/react-native'
import { PermissionsAndroid } from 'react-native';
import DeviceInfo from 'react-native-device-info';

export async function requestUserPermission() {
  let permissionGranted = false

  try {
    if (Platform.OS == 'android' && DeviceInfo.getApiLevelSync() >= 30) {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.POST_NOTIFICATION)

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        permissionGranted = true
      }
    } else {
      const authStatus = await messaging().requestPermission();
      permissionGranted =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    }

    notificationListeners()

  } catch (error) { }

  if (permissionGranted == true) {
    try {
      const getFcmToken = await messaging().getToken();
      // console.log('getFcmToken >>>', getFcmToken);
    } catch (error) { }
  }
}

export async function notificationListeners() {
  const unsubscribeMessage = messaging().onMessage(async remoteMessage => {
    await onDisplayNotification(remoteMessage);
    // console.log('Notification open from Foreground >>>>>');
  });

  const unsubscribeNotificationOpen = messaging().onNotificationOpenedApp(remoteMessage => {
    console.log('Notification open from Background State >>>>>');
    navigationService.navigate("ProfileScreen");
  });

  const initialNotification = await messaging().getInitialNotification();
  if (initialNotification) {
    console.log('Notification open from Quit state >>>>');
    navigationService.navigate("ProfileScreen");
    // navigationService.navigate(initialNotification?.data?.type);
  }
  return () => {
    unsubscribeMessage();
    unsubscribeNotificationOpen();
  };
}

// Show notification:
async function onDisplayNotification(data) {
  if (Platform.OS == 'ios') {
    await notifee.requestPermission();
  }
  // Create a channel (required for Android)
  const channelId = await notifee.createChannel({
    id: 'default',
    name: 'Default Channel',
    sound: 'default',
    importance: AndroidImportance.HIGH,
    badge: true
  });

  // Display a notification for Foreground
  await notifee.displayNotification({
    title: data?.notification?.title,
    body: data?.notification?.body,
    android: {
      channelId,
      pressAction: {
        launchActivity: "default",
        id: "default",
      },
      touchable: true,
    }
  })

  // Foreground trigger Event
  return notifee.onForegroundEvent(({ type, detail }) => {
    switch (type) {
      case EventType.DISMISSED:
        // console.log('User dismissed notification', detail.notification);

        break;
      case EventType.PRESS:
        // console.log('User pressed notification', detail.notification);
        navigationService.navigate("ProfileScreen");
        break;
    }
  });
}


//  navigationService.navigate(remoteMessage?.data?.type)

// {"name": "Vishal Ojha", "message": "This is test msg", "type": "ProfileScreen"}
