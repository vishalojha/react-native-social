import isaDate from '../../assets/isadate.svg'
import userAvtar from '../../assets/user5.svg'
import googleIcon from '../../assets/google.svg'
import addBtn from '../../assets/add-button.svg'
import backButton from '../../assets/back-btn.svg'
import sendIcon from '../../assets/send-button.svg'
import facebookIcon from '../../assets/facebook.svg'
import addFriendIcon from '../../assets/add-friend.svg'

const appIcons = {

  addBtn,
  isaDate,
  sendIcon,
  userAvtar,
  backButton,
  googleIcon,
  facebookIcon,
  addFriendIcon,

};

export default appIcons