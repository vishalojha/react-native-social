import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging'
import notifee from '@notifee/react-native'


// messaging().setBackgroundMessageHandler(async remoteMessage => {
//   console.log('Message handle in background >>>>>')
//   onDisplayNotification(remoteMessage)
// });

// async function onDisplayNotification(data) {
//   if (Platform.OS == 'ios') {
//     await notifee.requestPermission();
//   }
//   // Create a channel (required for Android)
//   const channelId = await notifee.createChannel({
//     id: 'default1',
//     name: 'Default Channel 1',
//     sound: 'default',
//     importance: AndroidImportance.HIGH,
//   });
//   // Display a notification
//   await notifee.displayNotification({
//     title: data?.notification?.title,
//     body: data?.notification?.body,
//     android: {
//       channelId,
//     }
//   })
// }


AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appName.toLowerCase(), () => App);

