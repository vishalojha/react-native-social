import 'react-native-gesture-handler';
import React from 'react';

import { LogBox } from 'react-native';
import AppNavigator from 'navigation/app';


const App = () => {
  LogBox.ignoreAllLogs();

  return (
    <AppNavigator />
  )
}

export default App
